var angularjs = require('orv.manager-angularjs');

angularjs.start({
	project: 'demo',
	paths: {
		build: 'build',
		index: 'www'
	}
});